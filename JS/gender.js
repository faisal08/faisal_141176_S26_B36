$(document).ready(function() {

    // Generate a simple captcha
    $('#genderform').bootstrapValidator({

        message: 'This value is not valid',

        fields: {
            username: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The username is required and can\'t be empty'
                    }
                }
            },
            gender: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The gender is required and can\'t be empty'
                    }

                }
            }
        }
    });
});