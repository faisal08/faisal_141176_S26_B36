<?php
namespace App\Birthdate;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
if(!isset($_SESSION))session_start();
//use PDO;

class Birthdate extends DB
{
    public $id;
    public $username;
    public $birthdate;

    public function __construct()
    {
        parent::__construct();

    }

    public function setData($postVariabledata = NULL)
    {
        if (array_key_exists("id", $postVariabledata)) {
            $this->id = $postVariabledata['id'];

        }
        if (array_key_exists("username", $postVariabledata)) {
            $this->username = $postVariabledata['username'];
        }
        if (array_key_exists("birthdate", $postVariabledata)) {
            $this->birthdate = $postVariabledata['birthdate'];
        }

    }
    public function store(){
        $arrData=array($this->username,$this->birthdate);
        $sql="insert into birthday(username,birthdate)VALUES
            (?,?)";
        $STH= $this->DBH->prepare($sql);
        $result= $STH->execute($arrData);
        if($result==NULL)
            Message::message("Failed! DATA HAS not BEEN INSERTED SUCCESSFULLY");
        else
            Message::message("Success! DATA HAS BEEN INSERTED SUCCESSFULLY");
        Utility::redirect('create.php');
    }
}




