<?php
namespace App\Summary_Of_Organization;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
if(!isset($_SESSION))session_start();
//use PDO;

class Summary_Of_Organization extends DB
{
    public $id;
    public $org_Name;
    public $org_Summary;

    public function __construct()
    {
        parent::__construct();

    }

    public function setData($postVariabledata = NULL)
    {
        if (array_key_exists("id", $postVariabledata)) {
            $this->id = $postVariabledata['id'];

        }
        if (array_key_exists("org_name", $postVariabledata)) {
            $this->org_Name = $postVariabledata['org_name'];
        }
        if (array_key_exists("org_summary", $postVariabledata)) {
            $this->org_Summary = $postVariabledata['org_summary'];
        }

    }
    public function store(){
        $arrData=array($this->org_Name,$this->org_Summary);
        $sql="insert into organization_summary(Org_Name,Org_Summary)VALUES
            (?,?)";
        $STH= $this->DBH->prepare($sql);
        $result= $STH->execute($arrData);
        if($result)
            Message::message("Success! DATA HAS BEEN INSERTED SUCCESSFULLY");
        else
            Message::message("Failed! DATA HAS not BEEN INSERTED SUCCESSFULLY");
        Utility::redirect('create.php');
    }
}




